import React from "react"
import { Helmet } from "react-helmet"

import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
  CssBaseline,
} from "@mui/material"

import { Routes } from "./routes"

const theme = responsiveFontSizes(
  createTheme({
    palette: {
      background: {
        default: "#191616",
      },
    },
  })
)

function App() {
  return (
    <>
      <Helmet>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
      </Helmet>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Routes />
      </ThemeProvider>
    </>
  )
}

export default App
