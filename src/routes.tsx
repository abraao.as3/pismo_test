import React, { Suspense, lazy } from "react"
import { BrowserRouter, Routes as ReactRoutes, Route } from "react-router-dom"

import { Loading } from "./shared/Loading"
import { Layout } from "./shared/Layout"

const Home = lazy(() => import("./pages/home/Home"))
const Neo = lazy(() => import("./pages/neo/Neo"))
const Blog = lazy(() => import("./pages/blog/Blog"))

const Routes = () => (
  <Suspense fallback={<Loading />}>
    <BrowserRouter>
      <ReactRoutes>
        <Route element={<Layout />}>
          <Route path="/" element={<Home />} />
          <Route path="/:page/:id" element={<Neo />} />
          <Route path="/blog" element={<Blog />} />
        </Route>
      </ReactRoutes>
    </BrowserRouter>
  </Suspense>
)

export { Routes }
