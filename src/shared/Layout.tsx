import React from "react"
import { Outlet } from "react-router"

import { Box } from "@mui/material"

const Layout = (): JSX.Element => (
  <Box
    component="main"
    sx={{
      width: "100vw",
      height: "100vh",
      overflowX: "hidden",
    }}
  >
    <Outlet />
  </Box>
)

export { Layout }
