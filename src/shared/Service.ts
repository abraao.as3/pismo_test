import axios from "axios"

const baseAPI = "https://api.nasa.gov"

export interface IError {
  error: any
}

export interface IUnit {
  estimated_diameter_max: number
  estimated_diameter_min: number
}

export interface IOrbitalData {}

export interface ICloseAproachData {
  close_approach_date: string
  close_approach_date_full: string
  epoch_date_close_approach: number
  miss_distance: {
    astronomical: string
    kilometers: string
    lunar: string
    miles: string
  }
  orbiting_body: string
}

export interface INeo {
  absolute_magnitude_h: number
  close_approach_data: ICloseAproachData[]
  designation: string
  estimated_diameter: {
    feet: IUnit
    kilometers: IUnit
    meters: IUnit
    miles: IUnit
  }
  id: string
  is_potentially_hazardous_asteroid: boolean
  is_sentry_object: boolean
  links: any
  name: string
  nasa_jpl_url: string
  neo_reference_id: string
  orbital_data: IOrbitalData
}

export interface INeos {
  element_count: number
  links: {
    next: string
    prev: string
    self: string
  }
  near_earth_objects: {
    [date: string]: INeo[]
  }
}

export const Service = {
  async getNeos(): Promise<IError | INeos> {
    try {
      const { data } = await axios.get(
        `${baseAPI}/neo/rest/v1/feed?start_date=2021-09-30&end_date=2021-09-30&api_key=DEMO_KEY`
      )

      return data as INeos
    } catch (err) {
      return { error: err } as IError
    }
  },

  async getNeo(id: number): Promise<IError | INeo> {
    try {
      const { data } = await await axios.get(
        `${baseAPI}/neo/rest/v1/neo/${id}?api_key=DEMO_KEY`
      )

      return data as INeo
    } catch (err) {
      return { error: err } as IError
    }
  },
}
