import React, { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"

import {
  Grid,
  Card,
  Box,
  CardContent,
  ButtonBase,
  Fade,
  Button,
} from "@mui/material"
import { red } from "@mui/material/colors"

import { Service, IError, INeos, INeo } from "../../shared/Service"

const Home = () => {
  const [neos, setNeos] = useState<INeo[] | null>(null)
  const [showDangerous, setShowDangerous] = useState(false)

  const navigate = useNavigate()

  useEffect(() => {
    getNeos()
  }, [])

  const getNeos = async () => {
    const resp = await Service.getNeos()

    if ((resp as IError).error) {
      console.log("erro")
      return
    }

    setNeos(Object.values((resp as INeos).near_earth_objects)[0])
  }

  const clickHandler = (neo: INeo) => () => {
    navigate(`/neo/${neo.id}`)
  }

  const gotoFormatingDate = () => {
    navigate(`/date/54192211`)
  }

  const dangerousHandler = () => {
    setShowDangerous(old => !old)
  }

  const gotoMissDistance = () => {
    navigate(`/distance/54194339`)
  }

  const gotoBlog = () => {
    navigate("/blog")
  }

  return (
    <Box p="20px" component="section">
      <Grid container spacing={2} component="section">
        {neos &&
          neos.map((neo: INeo) => (
            <Fade in={true} timeout={500}>
              <Grid item key={neo.id} xs={12} sm={6} md={4} component="section">
                <ButtonBase sx={{ width: 1 }} onClick={clickHandler(neo)}>
                  <Card
                    sx={{
                      width: 1,
                      backgroundColor:
                        showDangerous && neo.is_potentially_hazardous_asteroid
                          ? red[400]
                          : "#fff",
                      color:
                        showDangerous && neo.is_potentially_hazardous_asteroid
                          ? "#fff"
                          : "rgba(0, 0, 0, 0.87)",
                      transition: theme =>
                        theme.transitions.create(["color, background"]),
                    }}
                  >
                    <CardContent sx={{ "&:last-child": { p: "1rem" } }}>
                      {neo.name}
                    </CardContent>
                  </Card>
                </ButtonBase>
              </Grid>
            </Fade>
          ))}
      </Grid>
      <Box
        sx={{
          width: 1,
          display: "flex",
          justifyContent: "center",
          mt: "2rem",
          gap: "1rem",
        }}
      >
        <Button variant="contained" color="primary" onClick={gotoFormatingDate}>
          formating date
        </Button>
        <Button variant="contained" color="primary" onClick={dangerousHandler}>
          {showDangerous ? "hide" : "show"} dangerous
        </Button>
        <Button variant="contained" color="primary" onClick={gotoMissDistance}>
          miss distance
        </Button>
        <Button variant="contained" color="primary" onClick={gotoBlog}>
          blog
        </Button>
      </Box>
    </Box>
  )
}

export default Home
