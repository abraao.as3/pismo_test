import React from "react"
import sanitize from "sanitize-html"

import { Box } from "@mui/material"
import { red } from "@mui/material/colors"

const content = `<h1>This is a html Model</h1>
<img src="" onerror="javascript:alert('você foi hakeado')" />
<p>time de produtos Pismo</p>`

const Blog = () => {
  return (
    <Box
      sx={{
        p: "20px",
        "& h1": {
          color: red[400],
        },
        "& p": {
          color: "#fff",
        },
      }}
      dangerouslySetInnerHTML={{ __html: sanitize(content) }}
    ></Box>
  )
}

export default Blog
