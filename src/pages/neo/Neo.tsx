import React, { useEffect, useState } from "react"
import { useParams, useNavigate } from "react-router-dom"
import { format } from "date-fns"
import ptBR from "date-fns/locale/pt-BR"

import {
  Box,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Fade,
  Button,
} from "@mui/material"

import { red } from "@mui/material/colors"

import { Service, IError, INeo } from "../../shared/Service"

const Neo = () => {
  const { id, page } = useParams()
  const navigate = useNavigate()

  const [neo, setNeo] = useState<INeo | null>(null)
  const [minimumDistance, setMinimumDistance] = useState<number | null>(null)

  useEffect(() => {
    getNeo()
  }, [])

  useEffect(() => {
    if (page === "distance" && neo) {
      const dists = [...neo.close_approach_data]
      dists.sort((a, b) => {
        if (
          Number(a.miss_distance.kilometers) <
          Number(b.miss_distance.kilometers)
        )
          return -1
        if (
          Number(a.miss_distance.kilometers) >
          Number(b.miss_distance.kilometers)
        )
          return 1
        return 0
      })

      setMinimumDistance(Number(dists[0].miss_distance.kilometers))
    }
  }, [page, neo])

  const getNeo = async () => {
    const resp = await Service.getNeo(Number(id))

    if ((resp as IError).error) {
      console.log("erro")
      return
    }

    console.log(resp)

    setNeo(resp as INeo)
  }

  const backHandler = () => {
    navigate("/")
  }

  return (
    <Box sx={{ p: "20px" }}>
      {neo && (
        <Fade in={true} timeout={1000}>
          <Box>
            <Card>
              <CardHeader title={neo.name}></CardHeader>
              <CardContent>
                <Typography variant="body1">id: {neo.id}</Typography>
                <Typography variant="body1">
                  potentialy hazardous:{" "}
                  {String(neo.is_potentially_hazardous_asteroid)}
                </Typography>
                <Typography variant="body1">
                  absolute magnitude height: {neo.absolute_magnitude_h}
                </Typography>
                {page === "date" && (
                  <Typography variant="body1" sx={{ fontWeight: "bold" }}>
                    close aproach date:{" "}
                    {format(
                      new Date(
                        neo.close_approach_data[0].close_approach_date_full
                      ),
                      "PPPP",
                      { locale: ptBR }
                    )}
                  </Typography>
                )}
                {page === "distance" && minimumDistance && (
                  <Typography
                    variant="body1"
                    sx={{ fontWeight: "bold", color: red[400] }}
                  >
                    minimum miss distance: {minimumDistance}km
                  </Typography>
                )}
              </CardContent>
            </Card>

            <Box
              sx={{
                display: "flex",
                width: 1,
                justifyContent: "center",
                mt: "2rem",
              }}
            >
              <Button variant="contained" color="primary" onClick={backHandler}>
                Voltar
              </Button>
            </Box>
          </Box>
        </Fade>
      )}
    </Box>
  )
}

export default Neo
