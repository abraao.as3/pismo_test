# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/abraao.as3/pismo_test/compare/v1.0.0...v1.1.0) (2022-01-07)


### Features

* challenge four - sanitize ([93aef00](https://gitlab.com/abraao.as3/pismo_test/commit/93aef00a13d2073e81f24a2c0b1ab9fc614e963f))
* challenge one - date ([988e59d](https://gitlab.com/abraao.as3/pismo_test/commit/988e59dbea25e2ceb8bd100d26aaab25945909e2))
* challenge three - miss distance ([868806d](https://gitlab.com/abraao.as3/pismo_test/commit/868806df627a65c9de059e2afea9a43768c5ef6d))
* challenge two - dangerous ([497e440](https://gitlab.com/abraao.as3/pismo_test/commit/497e440271a24507b982e2be850f57a13fb0bd69))
* initialize first page ([43e92ad](https://gitlab.com/abraao.as3/pismo_test/commit/43e92ad5e1c00c1442cde7339c697043a73c7a93))
* showing neos ([26892fa](https://gitlab.com/abraao.as3/pismo_test/commit/26892faec669aa6218a8de7d23e3d599516c3d77))

## 1.0.0 (2022-01-05)

### Features

- initialize cra ([ab1695b](https://gitlab.com/abraao.as3/pismo_test/commit/ab1695b09905839ec58b77ebd16c82ed91793011))
- adding libs ([26127e9](https://gitlab.com/abraao.as3/pismo_test/commit/26127e9b7ba7d2c453eeca71e34bca409ed4e845))
